# README #

This is my codes for the big project on course [**Operating System**](http://acm.sjtu.edu.cn/wiki/Nachos_2014) in SJTU. In this project, I implemented all phases for NachOS project, a famous educational OS project developed by [CS162 at UC Berkeley](http://www.cs.berkeley.edu/~kubitron/courses/cs162-F05/Nachos/).

# Project Report:
http://kaichun-mo.com/resume/projects/nachos.pdf

# Contact:
Webpage: http://www.kaichun-mo.com