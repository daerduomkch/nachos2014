package nachos.userprog;

import nachos.filesys.RealFileSystem;
import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.UserKernel.memoryManager;

import java.io.EOFException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
//		This is original page allocation for one process, meaning vpn == ppn
//
//		int numPhysPages = Machine.processor().getNumPhysPages();
//		pageTable = new TranslationEntry[numPhysPages];
//		for (int i = 0; i < numPhysPages; i++)
//			pageTable[i] = new TranslationEntry(i, i, true, false, false, false);
		
		pid = pidCount++;
		pidTable.add(pid, this);
		++processAlive;
		
		fdt = new FileDescriptorTable();
		
		if(fm == null) {
			fm = new fileManagement();
		}
		
		//set the first user process to be the root process
		if(root == null) {
			root = this;
		}
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		if (!load(name, args))
			return false;

		thread = new UThread(this);
		thread.setName(name).fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();

		int vpn = getPageNumber(vaddr);
		if(vpn < 0) return 0;
		
		int location = vaddr - vpn * pageSize;
		
		int amount = 0;
		
		TranslationEntry translate = getTranslationEntry(vpn);
		
		while(amount < length && vpn < numPages && translate.valid) {
			int ppn = translate.ppn;
			
			translate.used = true;
			
			int tmpAmount = Math.min(length - amount, pageSize - location);
			System.arraycopy(memory, ppn * pageSize + location, data, offset, tmpAmount);
			amount += tmpAmount;
			
			++vpn; location = 0; offset += tmpAmount;
			translate = getTranslationEntry(vpn);
		}
		
		return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();

		int vpn = getPageNumber(vaddr);
		if(vpn < 0) return 0;
		
		int location = vaddr - vpn * pageSize;
		
		int amount = 0;
		
		TranslationEntry translate = getTranslationEntry(vpn);
		
		while(amount < length && vpn < numPages && !translate.readOnly && translate.valid) {
			int ppn = translate.ppn;
			
			translate.dirty = true;
			translate.used = true;
			
			int tmpAmount = Math.min(length - amount, pageSize - location);
			System.arraycopy(data, offset, memory, ppn * pageSize + location, tmpAmount);
			amount += tmpAmount;
			
			++vpn; location = 0; offset += tmpAmount;
			translate = getTranslationEntry(vpn);
		}
		
		return amount;
	}
	
	protected TranslationEntry getTranslationEntry(int vpn) {
		if(vpn < 0 || vpn >= numPages) return null;
		return pageTable[vpn];
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		numPages += stackPages;
		initialSP = numPages * pageSize;	//initial Stack Pointer

		// and finally reserve 1 page for arguments
		numPages++;

		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
					.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
//		if (numPages > Machine.processor().getNumPhysPages()) {
//			coff.close();
//			Lib.debug(dbgProcess, "\tinsufficient physical memory");
//			return false;
//		}
		
		memoryManager mm = UserKernel.getMemoryManager();

		//check whether there is enough space for us to load this new process
		if(!mm.hasFreeOfSize(numPages)) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}
		
		//initialize the pageTable
		pageTable = new TranslationEntry[numPages];
		
		int count = 0;
		
		// load sections
		//for each section
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			//for each page in this section
			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;

				//to implement
				// for now, just assume virtual addresses=physical addresses
				//copy the ith page in this section to physical page xxx(here, assume vpn=ppn)
				//section.loadPage(i, vpn);
				
				int ppn = mm.getFreePage();
				Lib.debug(dbgProcess, "\t ALLOCATION: "+vpn+" to "+ppn);
				pageTable[vpn] = new TranslationEntry(vpn, ppn, true, section.isReadOnly(), false, false);
				++count;
				
				section.loadPage(i, ppn);
			}
		}
		
		//need to load Stack Pages here
		//to implement
		for(int i=count; i<numPages; ++i) {
			int ppn = mm.getFreePage();
			Lib.debug(dbgProcess, "\t ALLOCATION: "+i+" to "+ppn);
			pageTable[i] = new TranslationEntry(i, ppn, true, false, false, false);
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the halt() system call.
	 */
	private int handleHalt() {

		if(root == this) {
			Machine.halt();
			
			Lib.assertNotReached("Machine.halt() did not halt machine!");
			return 0;
		}
		return -1;
	}
	
	private int handleExit(int status, boolean normal) {
		//close coff executable file
		coff.close();
		
		//close all file descriptors
		Lib.debug('f', "EXIT: pid = "+pid+", status = "+status+", parentPID = "+parentPid);
		fdt.closeAll(this);
		
		//clear all of its children's parent
		for(Integer ch: childrenPids) {
			int child = ch.intValue();
			pidTable.get(child).parentPid = -1;
		}
		
		//deal with exit status
		if(parentPid >= 0 && normal) {
			pidTable.get(parentPid).setExitStatus(pid, status);
		}
		
		//free up main memory
		freeUpSpatialResources();
		
		//termination of this process
		--processAlive;
		if(processAlive == 0) {
			Kernel.kernel.terminate();
		} else {
			UThread.finish();
		}
		
		Lib.assertNotReached("EXIT: exit() can return nothing!");
		return 0;
	}
	
	protected void freeUpSpatialResources() {
		for(int i=0; i<numPages; ++i) {
			int ppn = pageTable[i].ppn;
			UserKernel.getMemoryManager().returnPage(ppn);
		}
	}
	
	private int handleExec(int fileNameVMA, int argc, int argvs) {
		try{
			String[] args = new String[argc];
			for(int i=0; i<argc; ++i) {
				byte[] argAddr = new byte[addrSize];
				int amount = readVirtualMemory(argvs + i * addrSize, argAddr);
				
				if(amount != addrSize) {
					Lib.debug('f', "EXEC: cannot read the address of "+i+"th argument!");
					return -1;
				} else {
					int vaddr = Lib.bytesToInt(argAddr, 0);	//the ith argument string's start virtual address
					
					args[i] = readVirtualMemoryString(vaddr, MAXARGUMENTLENGTH);
					Lib.debug('f', "EXEC: content: "+args[i]);
				}
			}
			
			String fileName = readVirtualMemoryString(fileNameVMA, MAXARGUMENTLENGTH);
			
			Lib.debug('f', "EXEC: "+fileName+" with "+argc+" args, which are [");
			if(argc > 0) {
				for(int i=0; i<argc; ++i) {
					Lib.debug('f', "|"+args[i]+"|");
				}
			}
			Lib.debug('f',"]");
			
			UserProcess childProcess = UserProcess.newUserProcess();
			boolean successful = childProcess.execute(fileName, args);
			
			if(successful) {
				int cpid = childProcess.pid;
				childrenPids.add(new Integer(cpid));
				Lib.debug('f', "EXEC: current process's children are "+childrenPids);
				childProcess.parentPid = pid;
				
				Lib.debug('f', "EXEC: pid = "+cpid);
				return cpid;
			} else {
				--processAlive;
				Lib.debug('f', "EXEC: No file named "+fileName);
				return -1;
			}
		} catch(Exception e) {
			Lib.debug('f', "EXEC: Error Occurred!");
			return -1;
		}
	}
	
	private int handleJoin(int processID, int statusVMA) {
		Lib.debug('f', "JOIN: "+pid+" whose children are "+childrenPids+" join "+processID);
		
		UserProcess child = null;
		for(Integer cid: childrenPids) {
			if(cid.intValue() == processID) {
				child = pidTable.get(processID);
			}
		}
		
		if(child == null) return -1;
		
		child.thread.join();
		
		childrenPids.remove(new Integer(processID));
		
		Integer statusInteger = cpStatus.get(new Integer(processID));
		Lib.debug('f', "JOIN: status = "+statusInteger);
		if(statusInteger == null) return 0;
		
		int status = statusInteger.intValue();
		byte[] res = Lib.bytesFromInt(status);
		writeVirtualMemory(statusVMA, res);
		
		return 1;
	}
	
	private int handleCreate(int fileNameVMA) {
		try {
			String fileName = readVirtualMemoryString(fileNameVMA, MAXARGUMENTLENGTH);
			
			//System.out.print("\ncreate: "+fileName);
			fileName = convert(fileName);
			//System.out.println(" -> "+fileName);
			
			FileSystem fs = ThreadedKernel.fileSystem;
			
			if(!fm.toBeDeleted(fileName)) {
				
				OpenFile file = fs.open(fileName, true);
				
				if(file != null) {
					fm.acquire(file, fileName);
					int newfd = fdt.get(file);
					Lib.debug('f', "CREATE: "+newfd+"-> "+file+" "+fileName);
					return newfd;
				} else {
					Lib.debug('f', "CREATE: cannot create "+fileName);
					return -1;
				}
				
			} else {
				Lib.debug('f', "CREAT: cannot create a to-be-deleted file named "+fileName);
				return -1;
			}
		} catch(Exception e) {
			Lib.debug('f', "CREATE: Error Occurred!");
			return -1;
		}
	}
	
	private int handleOpen(int fileNameVMA) {
		try {
			String fileName = readVirtualMemoryString(fileNameVMA, MAXARGUMENTLENGTH);
			
			//System.out.print("\nopen: "+fileName);
			fileName = convert(fileName);
			//System.out.println(" -> "+fileName);
			
			FileSystem fs = ThreadedKernel.fileSystem;
			
			if(!fm.toBeDeleted(fileName)) {
				
				OpenFile file = fs.open(fileName, false);
				
				if(file != null) {
					fm.acquire(file, fileName);
					int newfd = fdt.get(file);
					Lib.debug('f', "OPEN: "+newfd+"-> "+file+" "+fileName);
					return newfd;
				} else {
					Lib.debug('f', "OPEN: cannot open "+fileName);
					return -1;
				}
				
			} else {
				Lib.debug('f', "OPEN: cannot open a to-be-deleted file named "+fileName);
				return -1;
			}
		} catch(Exception e) {
			Lib.debug('f', "OPEN: Error Occurred!");
			return -1;
		}
	}
	
	private int handleRead(int fd, int buffer, int count) {
		try{
			OpenFile file = fdt.search(fd);
			
			if(file != null) {
				byte[] tmp = new byte[count];
				int num = file.read(tmp, 0, count);
				if(num == -1) {
					Lib.debug('f', "READ: read failure when trying to read from "+file.getName());
					return -1;
				} else {
					Lib.debug('f', "READ: content: "+new String(tmp, Charset.forName("GBK"))+", num = "+num);
					writeVirtualMemory(buffer, tmp, 0, num);
					return num;
				}
			} else {
				Lib.debug('f', "READ: cannot open file descriptor "+fd);
				return -1;
			}
		} catch(Exception e) {
			Lib.debug('f', "READ: Error Occurred!");
			return -1;
		}
	}
	
	private int handleWrite(int fd, int buffer, int count) {
		try{
			OpenFile file = fdt.search(fd);
			
			if(file != null) {
				byte[] tmp = new byte[count];
				int num = readVirtualMemory(buffer, tmp);
				if(num == -1) {
					Lib.debug('f', "WRITE: read failure from address "+buffer);
					return -1;
				} else {
					Lib.debug('f', "WRITE: content: "+new String(tmp, Charset.forName("GBK"))+", num = "+num);
					file.write(tmp, 0, num);
					return num;
				}
			} else {
				Lib.debug('f', "WRITE: cannot open file descriptor "+fd);
				return -1;
			}
		} catch(Exception e) {
			Lib.debug('f', "WRITE: Error Occurred!");
			return -1;
		}
	}
	
	private int handleClose(int fd) {
		try {
			//System.out.println("close: fd = "+fd);
			OpenFile file = fdt.search(fd);
			
			if(file == null) {
				Lib.debug('f', "CLOSE: fd "+fd+" has not been used, cannot be closed!");
				return -1;
			} else {
				Lib.debug('f', "CLOSE: fd "+fd+" normal!");
				//System.out.println("close: "+file.getName());
				fm.release(file, file.getName());
				file.close();
				fdt.remove(fd);
				return 0;
			}
		} catch(Exception e) {
			Lib.debug('f', "CLOSE: Error Occurred!");
			return -1;
		}
	}
	
	/** return fileName with absolute path if using RealFileSystem
	 *  return itself is not using RealFileSystem
	 */
	private String convert(String fileName) {
		
		if(ThreadedKernel.fileSystem instanceof RealFileSystem) {
			RealFileSystem fs = (RealFileSystem) ThreadedKernel.fileSystem;
			return fs.getAbsoluteFilePath(fileName);
		}
		
		return fileName;
	}
	
	private int handleUnlink(int fileNameVMA) {
		
		try {
			
			String fileName = readVirtualMemoryString(fileNameVMA, MAXARGUMENTLENGTH);
			
			//System.out.print("\n\n"+fileName);
			fileName = convert(fileName);
			//System.out.println(" -> "+fileName);
			
			if(fm.isEmpty(fileName)) {
				if(fm.delete(fileName)) {
					return 0;
				}
				return -1;
			} else {
				if(fm.setToBeDeleted(fileName)) {
					return 0;
				}
				return -1;
			}
		} catch(Exception e) {
			Lib.debug('f', "UNLINK: Error Occurred!");
			return -1;
		}
	}
	
	// target only at phase 5, wrong but can pass the testcase!
	private int handleUnlink_Phase5(int fileNameVMA) {
		
		try {
			
			String fileName = readVirtualMemoryString(fileNameVMA, MAXARGUMENTLENGTH);
			
			//System.out.print("\n\n"+fileName);
			fileName = convert(fileName);
			//System.out.println(" -> "+fileName);
			
			//if(fm.isEmpty(fileName)) {
				if(fm.delete(fileName)) {
					return 0;
				}
				return -1;
			/*} else {
				if(fm.setToBeDeleted(fileName)) {
					return 0;
				}
				return -1;
			}*/
		} catch(Exception e) {
			Lib.debug('f', "UNLINK: Error Occurred!");
			return -1;
		}
	}

	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
     * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case syscallExit:
			Lib.debug('h',"handleExit");
			return handleExit(a0, true);
		case syscallExec:
			Lib.debug('h',"handleExec");
			return handleExec(a0, a1, a2);
		case syscallJoin:
			Lib.debug('h',"handleJoin");
			return handleJoin(a0, a1);
		case syscallHalt:
			Lib.debug('h',"handleHalt");
			return handleHalt();
		case syscallCreate:
			Lib.debug('h',"handleCreate");
			return handleCreate(a0);
		case syscallOpen:
			Lib.debug('h',"handleOpen");
			return handleOpen(a0);
		case syscallRead:
			Lib.debug('h',"handleRead");
			return handleRead(a0, a1, a2);
		case syscallWrite:
			Lib.debug('h',"handleWrite");
			return handleWrite(a0, a1, a2);
		case syscallClose:
			Lib.debug('h',"handleClose");
			return handleClose(a0);
		case syscallUnlink:
			Lib.debug('h',"handleUnlink");
			return handleUnlink_Phase5(a0);

		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			
			// first day modified
			handleExit(-1, false);
		}
		return 0;
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;
			
		case Processor.exceptionAddressError:
		case Processor.exceptionBusError:
		case Processor.exceptionIllegalInstruction:
		case Processor.exceptionOverflow:
		case Processor.exceptionPageFault:
		case Processor.exceptionReadOnly:
		case Processor.exceptionTLBMiss:
			//do not let the exception breakdown the kernel, just peacefully exit
			handleExit(-1, false);
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			Lib.assertNotReached("Unexpected exception");
		}
	}

	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	private int initialPC, initialSP;
	private int argc, argv;

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	
	//phase 2 task 3
	private static Vector<UserProcess> pidTable = new Vector<UserProcess>();
	private static int pidCount = 0;
	private static int processAlive = 0;
	
	public UThread thread;
	public int pid;
	private int parentPid = -1;
	private LinkedList<Integer> childrenPids = new LinkedList<Integer>();
	private HashMap<Integer, Integer> cpStatus = new HashMap<Integer, Integer>(); //children process exit status
	
	public boolean setExitStatus(int cpid, int status) {
		for(Integer cid: childrenPids) {
			if(cid.intValue() == cpid) {
				cpStatus.put(new Integer(cpid), new Integer(status));
				return true;
			}
		}
		return false;
	}
	
	//phase 2 task 1
	private class FileDescriptorTable {
		FileDescriptorTable() {
			fd = new HashMap<Integer, OpenFile>();
			for(int i=2; i<MAXOPENFILE; ++i) {
				fd.put(new Integer(i), null);
			}
			fd.put(new Integer(stdin), UserKernel.console.openForReading());
			fd.put(new Integer(stdout), UserKernel.console.openForWriting());
		}
		
		protected int get(OpenFile file) {
			for(int i=0; i<MAXOPENFILE; ++i) {
				if(fd.get(new Integer(i)) == null) {
					fd.put(new Integer(i), file);
					return i;
				}
			}
			return -1;
		}
		
		protected void remove(int f) {
			fd.put(new Integer(f), null);
		}
		
		protected OpenFile search(int f) {
			OpenFile file = fd.get(new Integer(f));
			return file;
		}
		
		protected void closeAll(UserProcess up) {
			for(int i=0; i<MAXOPENFILE; ++i) {
				OpenFile file = fd.get(new Integer(i));
				
				if(file != null) {
					fm.release(file, file.getName());
					file.close();
				}
			}
			
			fd = null;
		}
		
		private HashMap<Integer, OpenFile> fd;
	}
	
	private FileDescriptorTable fdt;
	
	private class fileManagement {
		fileManagement() {
			fileList = new HashMap<String, LinkedList<OpenFile>>();
			fileToBeDeleted = new LinkedList<String>();
		}
		
		//record the fact that OpenFile of opens a file named fileName
		public void acquire(OpenFile of, String fileName) {
			LinkedList<OpenFile> list = fileList.get(fileName);
			if(list == null) {
				LinkedList<OpenFile> tmpList = new LinkedList<OpenFile>();
				tmpList.add(of);
				fileList.put(fileName, tmpList);
			} else {
				list.add(of);
			}
		}
		
		//record the fact that OpenFile of closes a file named fileName
		public void release(OpenFile of, String fileName) {
			
			LinkedList<OpenFile> list = fileList.get(fileName);
			
			if(list == null) {
				return;		//cannot reach actually
			} else {
				list.remove(of);
				
				if(list.isEmpty() && toBeDeleted(fileName)) {
					delete(fileName);
				}
			}
		}
		
		//return true if the file named fileNamed is unused by any of opening OpenFile
		public boolean isEmpty(String fileName) {
			LinkedList<OpenFile> list = fileList.get(fileName);
			if(list == null) {
				return true;
			} else {
				return list.isEmpty();
			}
		}
		
		public boolean delete(String fileName) {
			fileToBeDeleted.remove(fileName);
			return ThreadedKernel.fileSystem.remove(fileName);
		}
		
		public boolean setToBeDeleted(String fileName) {
			if(!fileToBeDeleted.contains(fileName)) {
				fileToBeDeleted.add(fileName);
			}
			return true;
		}
		
		public boolean toBeDeleted(String fileName) {
			//System.out.println(fileToBeDeleted);
			return fileToBeDeleted.contains(fileName);
		}
		
		private HashMap<String, LinkedList<OpenFile>> fileList;
		private LinkedList<String> fileToBeDeleted;
	}
	
	private int getPageNumber(int vma) {
		if(vma < 0 || vma >= numPages * pageSize) {
			return -1;
		}
		return vma / pageSize;
	}
	
	private static fileManagement fm = null;
	private static UserProcess root = null;
	
	private static final int MAXARGUMENTLENGTH = 256;
	private static final int MAXOPENFILE = 16;
	private static final int addrSize = 4;
	private static final int stdin = 0;
	private static final int stdout = 1;
}
