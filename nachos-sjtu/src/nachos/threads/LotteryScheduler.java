package nachos.threads;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.threads.PriorityScheduler.PriorityQueue;
import nachos.threads.PriorityScheduler.ThreadState;

/**
 * A scheduler that chooses threads using a lottery.
 * 
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 * 
 * Meaning: pick a random lottery i among 1 to the sum of all waiting threads, and
 * choose the thread hold lottery i to dequeue.
 * 
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 * 
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking the
 * maximum).
 */
public class LotteryScheduler extends PriorityScheduler {
	/**
	 * Allocate a new lottery scheduler.
	 */
	public LotteryScheduler() {
	}

	/**
	 * Allocate a new lottery thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer tickets from
	 *            waiting threads to the owning thread.
	 * @return a new lottery thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new LotteryQueue(transferPriority);
	}
	
	//override the constant setups
	public static final int priorityDefault = 1;
	public static final int priorityMinimum = 1;
	public static final int priorityMaximum = Integer.MAX_VALUE;
	
	protected class LotteryQueue extends PriorityScheduler.PriorityQueue {
		LotteryQueue(boolean transferPriority) {
			super(transferPriority);
		}

		//override
		protected ThreadState pickNextThread() {
			//print();
			int[] tickets = new int[queue.size()];
			
			int k = 0, sum = 0;
			for(ThreadState ts: queue) {
				tickets[k] = ts.getEffectivePriority();
				sum += tickets[k];
				++k;
			}
			if(k == 0) return null;
			
			int winning = Lib.random(sum) + 1;
			int winner = 0;
			for(int i=0; i<k; ++i) {
				winning -= tickets[i];
				if(winning <= 0) {
					winner = i;
					break;
				}
			}
			
			return queue.get(winner);
		}
		
		public void print() {
			Lib.assertTrue(Machine.interrupt().disabled());
			// implement me (if you want)
			for(ThreadState ts: queue) {
				System.out.print(ts.thread+"("+ts.getEffectivePriority()+") ");
			}
			System.out.println();
		}
	}
	
	protected class LotteryThreadState extends PriorityScheduler.ThreadState {
		LotteryThreadState(KThread thread) {
			super(thread);
		}
		
		//override
		public int getEffectivePriority() {
			int res = priority;
			
			for(PriorityQueue pq: holderList) {
				if(pq.transferPriority) {
					for(ThreadState ts: pq.queue) {
						int tmp = ts.getEffectivePriority();
						res += tmp;
					}
				}
			}
			
			return res;
		}
	}
}
