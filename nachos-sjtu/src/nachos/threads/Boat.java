package nachos.threads;

import nachos.ag.BoatGrader;

public class Boat {
	static BoatGrader bg;
	
	//from A to B
	static int cOnA, aOnA;
	static int cOnB = 0, aOnB = 0;
	static boolean boatOnA = true; 	//Is boat at A?
	static int cOnBoat = 0;
	static boolean cGoBack = false; //whether one of the two children go back to A or not?
	
	static Lock A = new Lock();
	static Lock B = new Lock();
	
	static Condition cWaitOnA = new Condition(A);
	static Condition cReady = new Condition(A);
	static Condition cArriveB = new Condition(B);
	static Condition aArriveB = new Condition(B);
	static Condition aWaitOnA = new Condition(A);
	
	static Semaphore mainThread = new Semaphore(0);	//let mainThread sleep until all things done

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;

		// Instantiate global variables here
		cOnA = children; aOnA = adults;

		//System.out.println("START with "+cOnA+", "+aOnA);
		
		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.

		for(int i=0; i<children; ++i) {
			KThread newChild = new KThread(new Runnable() {
				public void run() {
					ChildItinerary();
				}
			});
			newChild.setName("Children " + (i + 1));
			newChild.fork();
		}
		
		for(int i=0; i<adults; ++i) {
			KThread newAdult = new KThread(new Runnable() {
				public void run() {
					AdultItinerary();
				}
			});
			newAdult.setName("Adult " + (i + 1));
			newAdult.fork();
		}
		
		//System.out.println(KThread.readyQueue);
		//System.out.println("STOP mainThread");
		mainThread.P();
		//System.out.println("All people are ferried to Molokai! Job done!");
	}

	static void AdultItinerary() {
		A.acquire();
		
		while(!boatOnA ||cOnA > 1) {
			//just sleep until the left one child wake me up
			aWaitOnA.sleep();
		}
		//just go to B

		--aOnA;
		boatOnA = false;
		
		//System.out.println("**"+KThread.currentThread());
		bg.AdultRowToMolokai();
		
		A.release();
		
		B.acquire();
		++aOnB;
		cArriveB.wake();
		aArriveB.sleep();
		B.release();
	}

	static void ChildItinerary() {
		//System.out.println(KThread.currentThread());
		
		while(cOnA + aOnA > 0) {
			A.acquire();
			//let adult go
			//System.out.println(cOnA + ", " + aOnA);
			if(cOnA == 1) {
				if(aOnA == 0) {
					//just travel back and we are done
					//System.out.println("cOnA = " + cOnA);
					--aOnA;
					boatOnA = false;
					//System.out.println("**" + KThread.currentThread());
					bg.ChildRowToMolokai();
					mainThread.V();
				}
				else {
					//wake up an adult and let him to go
					aWaitOnA.wake();
					cWaitOnA.sleep();
				}
			}
			//let children go
			else {
				//System.out.println("!!! cOnBoat = " + cOnBoat +", boatOnA = " + boatOnA);
				while(!boatOnA || cOnBoat >=2) {
					//just wait on A
					cWaitOnA.sleep();
				}
				//no one on boat, I can be one
				if(cOnBoat == 0) {
					++cOnBoat;
					//wake up another guy as a driver
					cWaitOnA.wake();
					//wait for him to drive
					//System.out.println(">>> On boat now! Go to sleep!");
					cReady.sleep();
					//System.out.println(">>> Let's go!");
					//System.out.println("**"+KThread.currentThread());
					bg.ChildRideToMolokai();
				}
				//already on guy on the boat, I can join him
				else {
					++cOnBoat;
					//System.out.println("**"+KThread.currentThread());
					bg.ChildRowToMolokai();
					//drive the passenger to B
					cReady.wake();
				}
				//someone arrive B
				--cOnBoat;
				--cOnA;
				boatOnA = false;
				A.release();
			
				B.acquire();
				//System.out.println("!!! cOnB = " + cOnB);
				++cOnB;
				
				if(cGoBack) {
					cGoBack = false;
				} else {
					cGoBack = true;
					cArriveB.sleep();
				}
				
				//just simply drive back to A
				//System.out.println("**" + KThread.currentThread());
				bg.ChildRowToOahu();
				--cOnB;
				B.release();
				
				A.acquire();
				++cOnA;
				boatOnA = true;
			}
			
			A.release();
		}
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}

}
