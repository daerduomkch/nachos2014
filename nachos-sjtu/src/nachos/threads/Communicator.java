package nachos.threads;

import nachos.machine.Lib;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		lock = new Lock();
		speak = new Condition(lock);
		listen = new Condition(lock);
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		lock.acquire();
		
		while(transferring || queue <= 0) {
			speak.sleep();
		}
		
		transferring = true;
		this.word = word;
		listen.wake();
		
		lock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		lock.acquire();
		
		++queue;
		speak.wake();
		
		listen.sleep();
		transferring = false;
		--queue;
		
		int res = word;
		
		speak.wake();
		
		lock.release();
		return res;
	}
	
	private Lock lock;
	private Condition speak, listen;
	private boolean transferring = false;
	private int queue = 0;
	private int word;
}
