package nachos.filesys;

import java.nio.charset.Charset;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
  INode inode;
  String nameWithAbsolutePath;
  
  private int pos;
  
  public File (INode inode)
  {
	  this.inode = inode;
	  pos = 0;
  }
  
  public int length ()
  {
    return inode.file_size;
  }
  
  public void setName(String newName) {
	  nameWithAbsolutePath = newName;
  }
  
  public String getName() {
	  if(nameWithAbsolutePath == null) return "unnamed";
	  return nameWithAbsolutePath;
  }
  
  public void close ()
  {
	  Lib.debug(dbgFS, "File.close: "+inode.use_count);
	  
	  // close the swapfile
	  if(inode.use_count == 0) {
		  inode.free();
	  }
	  
	   --inode.use_count;
	   
	   if(inode.use_count == 0) {
		   FilesysKernel.realFileSystem.removeINode(inode.getLocation());
		   inode.save();
	   }
  }
  
  public void seek (int pos)
  {
    this.pos = pos;
  }
  
  public int tell ()
  {
    return pos;
  }
  
  public int read (byte[] buffer, int start, int limit)
  {
    int ret = read(pos, buffer, start, limit);
    pos += ret;
    return ret;
  }
  
  public int write (byte[] buffer, int start, int limit)
  {
    int ret = write(pos, buffer, start, limit);
    pos += ret;
    return ret;
  }
  
  public int read (int pos, byte[] buffer, int start, int limit)
  {
	  Lib.debug('o', "File.read: filename = "+getName()+", pos = "+pos+", start = "+start+", limit = "+limit);
	  
	  if(pos + limit >= inode.file_size) {
		  limit -= pos + limit - inode.file_size;
	  }
	    
	  int sec_num = inode.getSector(pos, getName());
	  int offset = pos % Disk.SectorSize;
	  
	  int count = 0;
	  
	  while(limit > 0 && sec_num != -1) {
		  
		  byte[] tmp = new byte[Disk.SectorSize];
		  
		  Lib.debug('o', "File.read: sec_num = "+sec_num);
		  
		  Machine.synchDisk().readSector(sec_num, tmp, 0);
		  
		  int length = (Disk.SectorSize - offset > limit)? limit: (Disk.SectorSize - offset);
		  
		  Lib.debug('o', "File.read: filename = "+getName()+", start = "+start+", offset = "+offset+", length = "+length+", sec_num = "+sec_num);
		  
		  System.arraycopy(tmp, offset, buffer, start, length);
		  
		  limit -= length;
		  count += length;
		  start += length;
		  pos += length;
		  
		  sec_num = inode.getSector(pos, getName());
		  offset = 0;
	  }
	  
	  Lib.debug('o', "File.read: content = "+new String(buffer, Charset.forName("GBK")));

	  Lib.debug('o', "File.read: file = "+getName()+" read finished! count = "+count);
	  
	  return count;
  }
  
  public int write (int pos, byte[] buffer, int start, int limit)
  {
	  Lib.debug('o', "File.write: file = "+getName()+", content: "+new String(buffer, Charset.forName("GBK")));
	  
	  if(getName().intern() == "SWAP".intern()) Lib.debug(dbgFS, "SWAP > pos = "+pos+", limit = "+limit);
	  
	  inode.setFileSize(pos + limit);
	  
	  int sec_num = inode.getSector(pos, getName());
	  
	  int offset = pos % Disk.SectorSize;
	  
	  int count = 0;
	  
	  while(limit > 0 && sec_num != -1) {
		  
		  byte[] tmp = new byte[Disk.SectorSize];
		  
		  int length = (Disk.SectorSize - offset > limit)? limit: (Disk.SectorSize - offset);
		  
		  if(offset > 0) {
			  Machine.synchDisk().readSector(sec_num, tmp, 0);
		  }
		  
		  Lib.debug('o', "File.write: file = "+getName()+", start = "+start+", offset = "+offset+", length = "+length+", sec_num = "+sec_num);
		  
		  System.arraycopy(buffer, start, tmp, offset, length);
		  
		  Machine.synchDisk().writeSector(sec_num, tmp, 0);
		  
		  limit -= length;
		  count += length;
		  start += length;
		  pos += length;
		  
		  sec_num = inode.getSector(pos, getName());
		  offset = 0;
	  }
	  
	  Lib.debug('o', "File.write: file = "+getName()+" write finished! count = "+count);
	  
	  
	  return count;
  }
  
  private static final char dbgFS = 's';
}
