package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
  /** represent a system file (free list) */
  public static int TYPE_SYSTEM = 0;
  
  /** represent a folder */
  public static int TYPE_FOLDER = 1;
  
  /** represent a normal file */
  public static int TYPE_FILE = 2;
  
  /** represent a normal file that is marked as delete */
  public static int TYPE_FILE_DEL = 3;
  
  /** represent a symbolic link file */
  public static int TYPE_SYMLINK = 4;
  
  /** represent a folder that are not valid */
  public static int TYPE_FOLDER_DEL = 5;
  
  /** the reserve size (in byte) in the first sector */
  
  // 4 is for ext_addr
  private static final int FIRST_SEC_RESERVE = 16 + 4;	
  
  private static final int LinkedListMaxSecNum = (Disk.SectorSize - FIRST_SEC_RESERVE) / 4;
  
  /** size of the file in bytes */
  int file_size;
  
  /** the type of the file */
  int file_type;
  
  /** the number of programs that have access on the file */
  int use_count;
  
  /** the number of links on the file */
  int link_count;
  
  /** maintain all the sector numbers this file used in order */
  private LinkedList<Integer> sec_addr;
  
  /** the first address */
  private int addr;
  
  /** the extended address */
  //private LinkedList<Integer> addr_ext;
  private int addr_ext;
  private INode ext_inode = null;
  
  public INode (int addr)
  {
    file_size = 0;
    file_type = TYPE_FILE;
    use_count = 0;
    link_count = 0;
    sec_addr = new LinkedList<Integer>();
    this.addr = addr;
    addr_ext = -1;
  }
  
  /** get the sector number of a position in the file  */
  public int getSector (int pos, String name)
  {
	  if(name.intern() == "SWAP".intern()) Lib.debug(dbgFS, "INode.getSector: pos = "+pos+", addr_ext = "+addr_ext);
	  
	  if(pos >= file_size) {
		  Lib.debug(dbgFS, "INode.getSector: pos = "+pos+" >= file_size = "+file_size);
		  return -1;
	  }
	  
	  if(pos < LinkedListMaxSecNum * Disk.SectorSize) {
		  int res = sec_addr.get(pos / Disk.SectorSize);
		  if(name.intern() == "SWAP".intern()) Lib.debug(dbgFS, "> normal: "+res);
		  return res;
	  } else {
		  //Lib.debug(dbgFS, "pos = "+pos+", "+(pos - LinkedListMaxSecNum * Disk.SectorSize));
		  int res = ext_inode.getSector(pos - LinkedListMaxSecNum * Disk.SectorSize, name);
		  if(name.intern() == "SWAP".intern()) Lib.debug(dbgFS, "> extension: "+res);
		  return res;
	  }
  }
  
  /** change the file size and adjust the content in the inode accordingly */
  
  //TODO to implement when disk is full!!!!
  
  public void setFileSize (int size)
  {
	  Lib.debug(dbgFS, "INode.setFileSize: size = "+size+" file_size = "+file_size);
	  
	  if(size <= file_size) return;
	  
	  Lib.debug(dbgFS, "size = "+size+", file_size = "+file_size+", sec_addr = "+sec_addr+", list_size = "+sec_addr.size());
	  
	  int totalSecNum = getFileSecNum(file_size);
	  int enlarged_totalSecNum = getFileSecNum(size);
	  
	  Lib.debug(dbgFS, "INode.setFileSize: totalSecNum = "+totalSecNum+", enlarged_totalSecNum = "+enlarged_totalSecNum);
	  
	  if(totalSecNum > LinkedListMaxSecNum) {
		  ext_inode.setFileSize(size - LinkedListMaxSecNum * Disk.SectorSize);
	  } else {
		  
		  int total = (enlarged_totalSecNum > LinkedListMaxSecNum)? LinkedListMaxSecNum:enlarged_totalSecNum;
		  
		  for(int i=totalSecNum; i<total; ++i) {
			  int new_sec_num = FilesysKernel.realFileSystem.getFreeList().allocate();
			  
			  Lib.debug(dbgFS, "INode.setFileSize: new_sec_num = "+new_sec_num);
			  
			  sec_addr.add(new Integer(new_sec_num));
		  }
		  if(enlarged_totalSecNum > LinkedListMaxSecNum) {
			  
			  Lib.debug(dbgFS, "**************");
			  
			  addr_ext = FilesysKernel.realFileSystem.getFreeList().allocate();
			  ext_inode = FilesysKernel.realFileSystem.getINode(addr_ext);
			  ext_inode.setFileSize(size - LinkedListMaxSecNum * Disk.SectorSize);
		  }
	  }
	  
	  file_size = size;
	  
	  Lib.debug(dbgFS, "sec_addr = "+sec_addr+", list_size = "+sec_addr.size()+", file_size = "+file_size);
	  
	  save();
  }
  
  /** free the disk space occupied by the file (including inode) */
  public void free ()
  {
	  Lib.debug(dbgFS, "INode.free: ");
	  
	  for(Integer res: sec_addr) {
		  FilesysKernel.realFileSystem.getFreeList().deallocate(res.intValue());
	  }
	  
	  FilesysKernel.realFileSystem.getFreeList().deallocate(addr);
	  
	  if(ext_inode != null) ext_inode.free();
  }
  
  /** load inode content from the disk */
  public void load ()
  {
	  Lib.debug(dbgFS, "INode.load: addr = "+addr);
	  
	  byte[] buffer = new byte[Disk.SectorSize];
	  
	  Machine.synchDisk().readSector(addr, buffer, 0);
	  
	  file_size = Lib.bytesToInt(buffer, 0);
	  file_type = Lib.bytesToInt(buffer, 4);
	  use_count = Lib.bytesToInt(buffer, 8);
	  link_count = Lib.bytesToInt(buffer, 12);
	  addr_ext = Lib.bytesToInt(buffer, 16);
	  
	  Lib.debug(dbgFS, "INode.load: file_size = "+file_size+", file_type = "+file_type
	  		+ ", use_count = "+use_count+", link_count = "+link_count+", addr_ext = "+addr_ext);
	  
	  int totalSecNum = getFileSecNum(file_size);
	  int num = (totalSecNum > LinkedListMaxSecNum)?  LinkedListMaxSecNum: totalSecNum;
	  
	  sec_addr = new LinkedList<Integer>();
	  
	  for(int i=0; i<num; ++i) {
		  sec_addr.add(new Integer(Lib.bytesToInt(buffer, 20+4*i)));
	  }
	  
	  Lib.debug(dbgFS, "INode.load: sec_addr"+sec_addr);
	  
	  if(addr_ext >= 0) {
		  Lib.debug(dbgFS, "INode.save: addr_ext = "+addr_ext);
		  
		  ext_inode = FilesysKernel.realFileSystem.getINode(addr_ext);
		  ext_inode.load();
	  }
  }
  
  /** save inode content to the disk */
  public void save ()
  {
	  Lib.debug(dbgFS, "INode.save: addr = "+addr);
	  
	  byte[] buffer = new byte[Disk.SectorSize];
	  
	  Lib.bytesFromInt(buffer, 0, file_size);
	  Lib.bytesFromInt(buffer, 4, file_type);
	  Lib.bytesFromInt(buffer, 8, use_count);
	  Lib.bytesFromInt(buffer, 12, link_count);
	  Lib.bytesFromInt(buffer, 16, addr_ext);
	  
	  Lib.debug(dbgFS, "INode.save: file_size = "+file_size+", file_type = "+file_type
		  		+ ", use_count = "+use_count+", link_count = "+link_count+", addr_ext = "+addr_ext);
	  
	  int i = 0;
	  for(Integer res: sec_addr) {
		  
		  Lib.bytesFromInt(buffer, 20+4*i, res.intValue());
		  ++i;
	  }
	  
	  Lib.debug(dbgFS, "INode.save: sec_addr"+sec_addr);
	  
	  Machine.synchDisk().writeSector(addr, buffer, 0);
	  
	  if(addr_ext >= 0) {
		  Lib.debug(dbgFS, "INode.save: addr_ext = "+addr_ext);
		  
		  ext_inode.save();
	  }
  }
  
  public int getLocation() {
	  return addr;
  }
  
  public int getFileSecNum(int size) {
	  return Lib.divRoundUp(size, Disk.SectorSize);
  }
  
  private static final char dbgFS = 's';
}
