package nachos.filesys;

import java.util.LinkedList;
import nachos.machine.Disk;
import nachos.machine.Lib;

/**
 * FreeList is a single special file used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time (eg. when a file is deleted) for reuse in the future.
 * 
 * @author starforever
 */
public class FreeList extends File
{
  /** the static address */
  public static int STATIC_ADDR = 0;
  
  /** size occupied in the disk (bitmap) */
  static int size = Lib.divRoundUp(Disk.NumSectors, 8);
  
  /** maintain address of all the free sectors */
  private LinkedList<Integer> free_list;
  
  public FreeList (INode inode)
  {
    super(inode);
    free_list = new LinkedList<Integer>();
  }
  
  public void init ()
  {
	  Lib.debug(dbgFS, "FreeList.init: ");
	  
	  for (int i = 3; i < Disk.NumSectors; ++i)
		  free_list.add(i);
  }
  
  /** allocate a new sector in the disk */
  public int allocate ()
  {  
	  if(free_list.isEmpty()) return -1;
	 
	  int res = free_list.removeFirst().intValue();
	  
	  Lib.debug(dbgFS, "FreeList.allocate: "+res);
	  
	  return res;
  }
  
  /** deallocate a sector to be reused */
  public void deallocate (int sec)
  {
	  Lib.debug(dbgFS, "FreeList.deallocate: "+sec);
	  
	  free_list.add(new Integer(sec));
  }
  
  /** save the content of freelist to the disk */
  public void save ()
  {
	  Lib.debug(dbgFS, "FreeList.save: ");
	  
	  byte[] bitmap = new byte[size];
	  
	  for(Integer res: free_list) {
		  int offset = res.intValue() % 8;
		  int index = res.intValue() / 8;
		  bitmap[index] |= 1 << offset;
	  }
	  
	  write(0, bitmap, 0, size);
  }
  
  /** load the content of freelist from the disk */
  public void load ()
  {
	  Lib.debug(dbgFS, "FreeList.load: ");
	  
	  byte[] bitmap = new byte[size];
	  
	  read(0, bitmap, 0, size);
	  
	  for(int i=0; i<size; ++i) {
		  for(int j=0; j<8; ++j) {
			  if((Lib.bytesToInt(bitmap, i, 1)&(1<<j)) == 1) {
				  free_list.add(new Integer(8*i+j));
			  }
		  }
	  }
  }
  
  private static final char dbgFS = 's';
}
