package nachos.filesys;

import java.util.HashMap;
import java.util.LinkedList;

import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.threads.KThread;
import nachos.threads.Lock;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
  /** the free list */
  private FreeList free_list;
  
  /** the root folder */
  private Folder root_folder;
  
  /** the current folder */
  Folder cur_folder;
  
  /** the string representation of the current folder */
  LinkedList<String> cur_path = new LinkedList<String>();
  
  /**
   * initialize the file system
   * 
   * @param format
   *          whether to format the file system
   */
  public void init (boolean format)
  {
	  Lib.debug(dbgFS, "RealFileSystem.init: ");
	  
	if (format)
    {
		Lib.debug(dbgFS, "RealFileSystem.init: formatting!");
		
      INode inode_free_list = getINode(FreeList.STATIC_ADDR);
      inode_free_list.file_type = INode.TYPE_SYSTEM;
      free_list = new FreeList(inode_free_list);
      
      free_list.init();
      
      INode inode_root_folder = getINode(Folder.STATIC_ADDR);
      inode_root_folder.file_type = INode.TYPE_FOLDER;
      cur_folder = root_folder = new Folder(inode_root_folder);
      
      cur_folder.addEntry(".", Folder.STATIC_ADDR);
      cur_folder.addEntry("..", Folder.STATIC_ADDR);
      
      importStub();
      
      Lib.debug(dbgFS, "RealFileSystem.init: finish formatting!");
    }
    else
    {
    	Lib.debug(dbgFS, "RealFileSystem.init: loading!");
    	
      INode inode_free_list = getINode(FreeList.STATIC_ADDR);
      inode_free_list.load();
      free_list = new FreeList(inode_free_list);
      free_list.load();
      
      INode inode_root_folder = getINode(Folder.STATIC_ADDR);
      inode_root_folder.load();
      root_folder = new Folder(inode_root_folder);
      root_folder.load();
      
      cur_folder = root_folder;
      
      	Lib.debug(dbgFS, "RealFileSystem.init: finish loading!");
    }
	
  }
  
  public void finish ()
  {
	  Lib.debug(dbgFS, "RealFileSystem.finish: ");
	  
	  cur_folder.save();
	  cur_folder.inode.save();
	  free_list.save();
	  free_list.inode.save();
  }
  
  /** import from stub filesystem */
  private void importStub ()
  {
	  Lib.debug(dbgFS, "RealFileSystem.importStub: ");
	  
    FileSystem stubFS = Machine.stubFileSystem();
    FileSystem realFS = FilesysKernel.realFileSystem;
    String[] file_list = Machine.stubFileList();
    for (int i = 0; i < file_list.length; ++i)
    {
      if (!file_list[i].endsWith(".coff"))
        continue;
    	
      System.out.println("RealFileSystem.importStub: transmit "+file_list[i]);
      Lib.debug(dbgFS, "RealFileSystem.importStub: transmit "+file_list[i]);
      
      OpenFile src = stubFS.open(file_list[i], false);
      if (src == null)
      {
        continue;
      }
      OpenFile dst = realFS.open(file_list[i], true);
      int size = src.length();
      byte[] buffer = new byte[size];
      src.read(0, buffer, 0, size);
      dst.write(0, buffer, 0, size);
      src.close();
      dst.close();
    }
  }
  
  /** get the only free list of the file system */
  public FreeList getFreeList ()
  {
    return free_list;
  }
  
  /** get the only root folder of the file system */
  public Folder getRootFolder ()
  {
    return root_folder;
  }
  
  public String getCurrentFolder() {
	  
	  String res = "";
	  for(String p: cur_path) {
		  res += "/" + p;
	  }
	  
	  if(res.intern() == "".intern()) res = "/";
	  
	  return res;
  }
  
  public OpenFile open(String name, boolean create) {
	 
	 // deal with swapfile, not in root directory of course!
	 if(FilesysKernel.realFileSystem == null) {
			  
		 Lib.debug(dbgFS, "RealFileSystem.open: initializing swapfile!");
			  
		 swapfile_inode = getINode(2);
		 swapfile_inode.file_type = INode.TYPE_SYSTEM;
		 swapfile_inode.link_count = 1;
			  
		 swapfile = new File(swapfile_inode);
		 swapfile.setName("SWAP");
		 return swapfile;
	 }
	  
	  Pair pair = splitPath(name);
	  String path = pair.path; name = pair.name;
	  
	  String path_backup = getCurrentFolder();
	  
	  if(!changeCurFolder(path)) return null;
	  
	  OpenFile res = openInCurrentFolder(name, create);
	  
	  changeCurFolder(path_backup);
	  
	  return res;
  }
  
  // file name is in current folder
  private OpenFile openInCurrentFolder (String name, boolean create)
  {
	  
	  Lib.debug(dbgFS, "RealFileSystem.open: filename = "+name);
	  
	  Lib.debug(dbgFS, "RealFileSystem.open: disk data: ");
	  if(Lib.test(dbgFS)) printCurrentDir();
	  
	  FolderEntry fe = getEntry(name);
	  Lib.debug(dbgFS, "RealFileSystem.open: fe = " + fe);
	  
	  if(fe != null) {
		  int addr = fe.addr;
		  INode inode = getINode(addr);
		  if(inode.file_type != INode.TYPE_FILE && inode.file_type != INode.TYPE_SYMLINK) return null;
	  }
	  
	  if(create) {
		  
		  Lib.debug(dbgFS, "RealFileSystem.open: create file "+name);
		  
		  if(fe != null) removeInCurrentFolder(name);
		  
		  int new_sec_num = free_list.allocate();
		  if(new_sec_num < 0) return null;
		  
		  INode inode = getINode(new_sec_num);
		  inode.use_count = 1;
		  inode.link_count = 1;
		  inode.save();
		  
		  cur_folder.addEntry(name, new_sec_num);
		  
		  File file = new File(inode);
		  file.setName(getAbsoluteFilePath(name));
		  
		  return file;
	  }
	  
	  if(fe != null) {
		  
		  Lib.debug(dbgFS, "RealFileSystem.open: open file "+name);
		  
		  INode inode = getINode(fe.addr);
		  inode.load();
		  ++inode.use_count;
		  inode.save();
		  
		  File file = new File(inode);
		  file.setName(getAbsoluteFilePath(name));
		  
		  if(inode.file_type == INode.TYPE_SYMLINK) {
			  byte[] buffer = new byte[inode.file_size];
			  file.read(buffer, 0, inode.file_size);
			  String path = Lib.bytesToString(buffer, 0, inode.file_size);
			  return open(path, false);
		  }
		  
		  Lib.debug(dbgFS, "RealFileSystem.open: open file "+name+" finished!");
		  
		  return file;
	  }
	  
	  return null;
  }
  
  // name can be a path
  public boolean remove(String name) {
	 
	  Pair pair = splitPath(name);
	  String path = pair.path; name = pair.name;
	  
	  String path_backup = getCurrentFolder();
	  
	  if(!changeCurFolder(path)) return false;
	  
	  boolean res = removeInCurrentFolder(name);
	  
	  changeCurFolder(path_backup);
	  
	  return res;
  }
  
  // file name is in current folder
  private boolean removeInCurrentFolder (String name)
  {
	  FolderEntry fe = getEntry(name);
	  
	  if(fe == null) return false;
	  
	  cur_folder.removeEntry(name);
	  
	  INode inode = getINode(fe.addr);
	  inode.load();
	  
	  --inode.link_count;
	  
	  if(inode.link_count == 0) {
		  inode.free();
	  } else {
		  inode.save();
	  }
	  
	  return true;
  }
  
  public boolean createFolder (String name)
  {
	  if(cur_folder.exists(name)) return false;
	  
	  int new_sec_num = free_list.allocate();
	  if(new_sec_num < 0) return false;
	  
	  INode inode = getINode(new_sec_num);
	  inode.file_type = INode.TYPE_FOLDER;
	  
	  Folder folder = new Folder(inode);
	  folder.addEntry(".", new_sec_num);
	  folder.addEntry("..", cur_folder.inode.getLocation());
	  
	  folder.save();
	  inode.save();
	  
	  cur_folder.addEntry(name, new_sec_num);
	  
	  return true;
  }
  
  public boolean removeFolder (String name)
  {
	  
	  FolderEntry fe = cur_folder.getEntry(name);
	  if(fe == null) return false;
	  
	  INode inode = getINode(fe.addr);
	  inode.load();
	  
	  if(inode.file_type != INode.TYPE_FOLDER) return false;
	  
	  Folder folder = new Folder(inode);
	  folder.load();
	  
	  if(folder.getEntryNames().length > 2) return false;
	  
	  inode.free();
	  cur_folder.removeEntry(name);
	  
	  return true;
  }
  
  public boolean changeCurFolder (String name)
  {
	 
	  // back up in case of invalid change folder operation execute
	  Folder cur_folder_backup = cur_folder;
	  LinkedList<String> cur_path_backup = new LinkedList<String>();
	  for(String str: cur_path) {
		  cur_path_backup.add(str);
	  }
	  
	  LinkedList<String> list = new LinkedList<String>();
	  boolean isAbsolute = parsePath(name, list);
	  
	  cur_folder.save();
	  cur_folder.inode.save();
	  
	  if(isAbsolute) {
		  
		  cur_path = new LinkedList<String>();
		  
		  INode root_inode = getINode(Folder.STATIC_ADDR);
		  root_inode.load();
		  
		  cur_folder = new Folder(root_inode);
		  cur_folder.load();
	  }
	  
	  boolean success = true;
	  
	  for(String str: list) {
		  
		  if(str.intern() == "..".intern() && !cur_path.isEmpty()) {
			  cur_path.removeLast();
		  } else if(str.intern() != ".".intern()) {
			  cur_path.add(str);
		  }
		  
		  FolderEntry fe = cur_folder.getEntry(str);
		  
		  if(fe == null) {
			  success = false;
			  break;
		  }
		  
		  INode inode = getINode(fe.addr);
		  inode.load();
		  
		  if(inode.file_type != INode.TYPE_FOLDER) {
			  success = false;
			  break;
		  }
		  
		  cur_folder = new Folder(inode);
		  cur_folder.load();
	  }
	  
	  if(!success) {
		  cur_folder = cur_folder_backup;
		  cur_path = cur_path_backup;
	  }
	  
	  return success;
  }
  
  public FileStat getStat (String name)
  {
	  FolderEntry fe = getEntry(name);
	  if(fe == null) return null;
	  
	  int addr = fe.addr;
	  INode inode = getINode(addr);
	  inode.load();
	  
	  FileStat res = new FileStat();
	  res.inode = addr;
	  res.links = inode.link_count;
	  res.name = name;
	  res.sectors = inode.getFileSecNum(inode.file_size);
	  res.size = inode.file_size;
	  if(inode.file_type == INode.TYPE_FILE) res.type = FileStat.NORMAL_FILE_TYPE;
	  if(inode.file_type == INode.TYPE_FOLDER) res.type = FileStat.DIR_FILE_TYPE;
	  if(inode.file_type == INode.TYPE_SYMLINK) res.type = FileStat.LinkFileType;
	  
	  return res;
  }
  
  // file src is in current folder
  public boolean createLink (String src, int addr)
  {
	  if(cur_folder.exists(src)) return false;
	  
	  INode inode = getINode(addr);
	  inode.load();
	  ++inode.link_count;
	  inode.save();
	  
	  cur_folder.addEntry(src, addr);
	  
	  return true;
  }
  
  // file src is in current folder
  public boolean createSymlink (String src, String dst)
  {
	  if(cur_folder.exists(src)) return false;
	  
	  int new_sec_num = free_list.allocate();
	  if(new_sec_num < 0) return false;
	  
	  cur_folder.addEntry(src, new_sec_num);
	  
	  INode new_inode = getINode(new_sec_num);
	  new_inode.file_type = INode.TYPE_SYMLINK;
	  new_inode.save();
	  
	  File symlink_file = new File(new_inode);
	  symlink_file.setName(getAbsoluteFilePath(src));
	  
	  dst = getAbsoluteFilePath(dst);
	  byte[] buffer = dst.getBytes();
	  
	  int count = symlink_file.write(buffer, 0, buffer.length);
	  if(count != buffer.length) return false;
	  
	  return true;
  }
  
  public int getFreeSize()
  {
    //TODO implement this
    return 0;
  }
  
  public int getSwapFileSectors()
  {
    //TODO implement this
    return 0;
  }
  
  // return whether is absolute path or not
  private boolean parsePath(String name, LinkedList<String> list) {
	  
	  if(name.length() == 0) return false;
	  
	  char[] path = name.toCharArray();

	  int index = 0;

	  boolean isAbsolute = false;
	  if(path[0] == '/') {
		  isAbsolute = true;
		  index = 1;
	  }

	  String tmp = "";
	  while(index < path.length) {
		  if(path[index] == '/') {
			  if(tmp.intern() == "".intern()) {
				  tmp=".";
			  }
			  list.add(tmp);
			  tmp = "";
		  } else {
			  tmp += path[index];
		  }

		  ++index;
	  }

	  if(tmp != null) {
		  if(tmp.intern() == "".intern()) {
			  tmp=".";
		  }
		  list.add(tmp);
	  }

	  return isAbsolute;
  }
  
  public Pair splitPath(String path) {
	  
	  char[] p = path.toCharArray();
	  int index = p.length - 1;
	  
	  while(index >= 0 && p[index] == '/') {
		  --index;
	  }
	  
	  String dirName = "";
	  while(index >= 0 && p[index] != '/') {
		  dirName = p[index] + dirName;
		  --index;
	  }
	  
	  String frontPath = "";
	  if(index == 0) {
		  frontPath = "/";
	  }
	  if(index > 0) {
		  frontPath = path.substring(0, index);
	  }
	  
	  return new Pair(dirName, frontPath);
  }
  
  
  public String getAbsoluteFilePath(String name) {
	  
	  if(name.charAt(0) != '/') 	//relative path
	  {
		  RealFileSystem fs = FilesysKernel.realFileSystem;
		  
		  String cur_path = fs.getCurrentFolder();
		  
		  if(cur_path.intern() == "/".intern()) return cur_path + name;
		  else return cur_path + "/" + name;
	  } 
	  else		//absolute path
	  {
		  return name;
	  }
  }

  public String[] readCurrentDir() {
	  return cur_folder.getEntryNames();
  }
  
  public FolderEntry getEntry(String name) {
	  return cur_folder.getEntry(name);
  }
  
  
  public INode getINode(int addr) {
	  
	  INode res = inodeMap.get(new Integer(addr));
	  
	  if(res == null) {
		  res = new INode(addr);
		  inodeMap.put(new Integer(addr), res);
	  }
	  
	  return res;
  }
  
  public void removeINode(int addr) {
	  
	  inodeMap.remove(new Integer(addr));
  }
  
  HashMap<Integer, INode>  inodeMap = new HashMap<Integer, INode>();
  
  // for debugging
  private void printCurrentDir() {
	  String[] res = readCurrentDir();
	  if(res == null) {
		  System.out.println("empty!");
		  return;
	  }
	  for(int i=0; i<res.length; ++i) {
		  System.out.println(i+": "+res[i]);
	  }
  }
  
  private static final char dbgFS = 's';
  private static File swapfile;
  private static INode swapfile_inode;
}
