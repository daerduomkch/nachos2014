package nachos.filesys;

import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.Iterator;

import nachos.machine.Lib;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
  /** the static address for root folder */
  public static int STATIC_ADDR = 1;
  
  private int size;
  
  /** mapping from filename to folder entry */
  private Hashtable<String, FolderEntry> entry;
  
  public Folder (INode inode)
  {
    super(inode);
    size = 4;	// total number of hash mappings
    entry = new Hashtable<String, FolderEntry>();
  }
  
  /** open a file in the folder and return its address */
  public int open (String filename)
  {
	  FolderEntry fe = entry.get(filename);
	  
	  if(fe == null) {
		  Lib.debug(dbgFS, "Folder.open: failure, file "+filename+" does not exist!");
		  return -1;
	  }
	  
	  Lib.debug(dbgFS, "Folder.open: success, file "+filename+" is in "+fe.addr+" sector!");
	  
	  return fe.addr;
  }
  
  public FolderEntry getEntry(String name) {
	  
	  return entry.get(name);
  }
  
  /** create a new file in the folder and return its address */
  public int create (String filename)
  {
	  FolderEntry fe = entry.get(filename);
	  
	  if(fe != null) {
		  Lib.debug(dbgFS, "Folder.create: failure, file "+filename+" already exists!");
		  return -1;
	  }
	  
	  int new_sec_num = FilesysKernel.realFileSystem.getFreeList().allocate();
	  addEntry(filename, new_sec_num);
	  
	  Lib.debug(dbgFS, "Folder.create: success, file "+filename+" is given "+new_sec_num+" sector to use!");
	  
	  return new_sec_num;
  }
  
  /** add an entry with specific filename and address to the folder */
  public void addEntry (String filename, int addr)
  {
	  //Lib.debug(dbgFS, "Folder.addEntry: "+filename+", "+addr);
	  
	  if(entry.get(filename) == null) {
		  
		  entry.put(filename, new FolderEntry(filename, addr));
		  size += 4 + filename.length() + 4;
		  
//		  inode.save();
//		  save();
		  
	  } else {
		  Lib.debug(dbgFS, "Folder.addEntry: failure: "+filename+" already exists!");
	  }
  }
  
  /** remove an entry from the folder */
  public void removeEntry (String filename)
  {
	  Lib.debug(dbgFS, "Folder.removeEntry: "+filename);
	  
	  FolderEntry fe = entry.remove(filename);
	  
	  if(fe != null) {
		  size -= 4 + filename.length() + 4;

//		  inode.save();
//		  save();
		  
	  } else {
		  Lib.debug(dbgFS, "Folder.removeEntry: failure: "+filename+" does not exist!");
	  }
  }
  
  public boolean exists(String str) {
	  return (entry.get(str) != null);
  }
  
  /** save the content of the folder to the disk */
  public void save ()
  {
	  Lib.debug(dbgFS, "Folder.save: size = "+size+" entry = "+entry);
	  
	  byte[] buffer = new byte[size];
	  
	  Lib.bytesFromInt(buffer, 0, entry.size());
	  
	  int pos = 4;
	  Iterator<String> ite = entry.keySet().iterator();
	  
	  while(ite.hasNext()) {
		  
		  String filename = (String) ite.next();
		  int addr = entry.get(filename).addr;
		  
		  Lib.bytesFromInt(buffer, pos, addr); pos +=4;
		  Lib.bytesFromInt(buffer, pos, filename.length()); pos += 4;
		  
		  byte[] tmp = filename.getBytes();
		  for(int i=0; i<tmp.length; ++i) {
			  buffer[pos] = tmp[i]; ++pos;
		  }
	  }
	  
	  write(0, buffer, 0, size);
  }
  
  /** load the content of the folder from the disk */
  public void load ()
  {
	  Lib.debug(dbgFS, "Folder.load: ");
	  
	  byte[] buffer = new byte[inode.file_size];
	  
	  read(0, buffer, 0, inode.file_size);
	  
	  int tot = Lib.bytesToInt(buffer, 0);
	  
	  int pos = 4;
	  
	  for(int i=0; i<tot; ++i) {
		  
		  int addr = Lib.bytesToInt(buffer, pos); pos += 4;
		  int length = Lib.bytesToInt(buffer, pos); pos += 4;
		  String filename = Lib.bytesToString(buffer, pos, length); pos += length;
		  
		  addEntry(filename, addr);
	  }
  }

  public String[] getEntryNames() {
	 
	  if(entry.size() == 0) return null;
	  
	  String[] res = new String[entry.size()];
	  
	  Iterator<String> ite = entry.keySet().iterator();
	  int i=0;
	  while(ite.hasNext()) {
		  res[i] = ite.next();
		  ++i;
	  }
	  
	  return res;
  }
  
  private static final char dbgFS = 's';
}
