package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.threads.KThread;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
  protected static final int SYSCALL_MKDIR = 14;
  protected static final int SYSCALL_RMDIR = 15;
  protected static final int SYSCALL_CHDIR = 16;
  protected static final int SYSCALL_GETCWD = 17;
  protected static final int SYSCALL_READDIR = 18;
  protected static final int SYSCALL_STAT = 19;
  protected static final int SYSCALL_LINK = 20;
  protected static final int SYSCALL_SYMLINK = 21;
  
  @Override
  public void saveState() {
	  
	  //System.out.println(KThread.currentThread()+ "FilesysProcess.saveState: ");
	  
	  super.saveState();
	  
	  cur_folder_backup = FilesysKernel.realFileSystem.cur_folder;
	  cur_path_backup = FilesysKernel.realFileSystem.cur_path;
	  //cur_path = FilesysKernel.realFileSystem.getCurrentFolder();
	  
	  Lib.debug(dbgFS, "FilesysProcess.saveState: kthread = "+KThread.currentThread()+", cur_path = $"+cur_path_backup+"$");
  }
  
  @Override
  public void restoreState() {
	  
	  //System.out.println(KThread.currentThread()+ "FilesysProcess.restoreState: ");
	  
	  super.restoreState();
	  
	  if(cur_path_backup == null) {
		  return;
	  }
	  
	  FilesysKernel.realFileSystem.cur_folder = cur_folder_backup;
	  FilesysKernel.realFileSystem.cur_path = cur_path_backup;
	  
	  Lib.debug(dbgFS, "FilesysProcess.restoreState: kthread = "+KThread.currentThread()+",  cur_path = $"+cur_path_backup+"$");
	  
	  //FilesysKernel.realFileSystem.changeCurFolder(cur_path);
  }
  
  private int handleMkdir(int pathVMA) {
	  
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String path = readVirtualMemoryString(pathVMA, MAXPATHLENGTH);
	  
	  Pair res = fs.splitPath(path);
	  
	  String dirName = res.name;
	  path = res.path;
	 
	  if(res.name.intern() == "".intern()) return -1;
	  
	  String path_backup = fs.getCurrentFolder();
	  
	  if(!fs.changeCurFolder(path)) return -1;
	  
	  if(!fs.createFolder(dirName)) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	 
	  fs.changeCurFolder(path_backup);
	  
	  return 0;
  }
  
  private int handleRmdir(int pathVMA) {	  
	  
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String path = readVirtualMemoryString(pathVMA, MAXPATHLENGTH);
	  
	  Pair res = fs.splitPath(path);
	  
	  String dirName = res.name;
	  path = res.path;
	  
	  if(res.name.intern() == "".intern()) return -1;
	  
	  String path_backup = fs.getCurrentFolder();
	  
	  if(!fs.changeCurFolder(path)) return -1;
	  
	  if(!fs.removeFolder(dirName)) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	  
	  fs.changeCurFolder(path_backup);
	  
	  return 0;
  }

  private int handleChdir(int pathVMA) {
	
	String path = readVirtualMemoryString(pathVMA, MAXPATHLENGTH);
	
	RealFileSystem fs = FilesysKernel.realFileSystem;
	
	Lib.debug(dbgFS, "FilesysProcess.handleChdir: path = "+path);
	
	if(!fs.changeCurFolder(path)) return -1;
	
	return 0;
  }
  
  private int handleGetcwd(int bufVMA, int size) {
	  
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String res = fs.getCurrentFolder();
	  
	  Lib.debug(dbgFS, "FilesysProcess.handleGetcwd: res.length = "+res.length()+", size = "+size);
	  
	  if(res.length() > size) return -1;
	  
	  byte[] buffer = new byte[size];
	  System.arraycopy(res.getBytes(), 0, buffer, 0, res.length());
	  writeVirtualMemory(bufVMA, buffer);
	  
	  return res.length();
  }
  
  private int handleReaddir(int dirNameVMA, int bufVMA, int size, int namesize) {
	  
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String dirName = readVirtualMemoryString(dirNameVMA, MAXPATHLENGTH);
	  
	  String cur_path_backup = fs.getCurrentFolder();
	  
	  if(!fs.changeCurFolder(dirName)) return -1;
	  
	  String[] res = fs.readCurrentDir();
	  
	  if(res == null) {
		  fs.changeCurFolder(cur_path_backup);
		  return 0;
	  }
	  
	  int length = (res.length > size)? size : res.length;
	  
	  for(int i=0; i<length; ++i) {
		  
		  if(res[i].length() > namesize) {
			  fs.changeCurFolder(cur_path_backup);
			  return -1;
		  }
		  
		  byte[] tmp = new byte[namesize];
		  System.arraycopy(res[i].getBytes(), 0, tmp, 0, res[i].length());
		  
		  writeVirtualMemory(bufVMA, tmp);
		  
		  bufVMA += namesize;
	  }
	  
	  fs.changeCurFolder(cur_path_backup);
	  
	  return length;
  }
  
  private int handleStat(int fileNameVMA, int statVMA) {
	  
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String fileName = readVirtualMemoryString(fileNameVMA, MAXPATHLENGTH);	
	  
	  byte[] res = new byte[FileStat.FILE_NAME_MAX_LEN + 20];
	  
	  Pair pair = fs.splitPath(fileName);
	  
	  String cur_path_backup = fs.getCurrentFolder();
	  
	  if(!fs.changeCurFolder(pair.path)) return -1;
	  
	  FileStat filestat = fs.getStat(pair.name);
	  
	  fs.changeCurFolder(cur_path_backup);
	  
	  byte[] tmp = fileName.getBytes();
	  int length = (tmp.length > FileStat.FILE_NAME_MAX_LEN)? FileStat.FILE_NAME_MAX_LEN: tmp.length;
	  System.arraycopy(tmp, 0, res, 0, length);
	  
	  int pos = FileStat.FILE_NAME_MAX_LEN;
	  Lib.bytesFromInt(res, pos, filestat.size); pos += 4;
	  Lib.bytesFromInt(res, pos, filestat.sectors); pos += 4;
	  Lib.bytesFromInt(res, pos, filestat.type); pos += 4;
	  Lib.bytesFromInt(res, pos, filestat.inode); pos += 4;
	  Lib.bytesFromInt(res, pos, filestat.links);
	  
	  writeVirtualMemory(statVMA, res);
	  
	  return 0;
  }
  
  private int handleLink(int oldNameVMA, int newNameVMA) {
	  
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String oldName = readVirtualMemoryString(oldNameVMA, MAXPATHLENGTH);
	  String newName = readVirtualMemoryString(newNameVMA, MAXPATHLENGTH);
	  
	  Pair oldPair = fs.splitPath(oldName);
	  Pair newPair = fs.splitPath(newName);
	  
	  String path_backup = fs.getCurrentFolder();
	  
	  if(!fs.changeCurFolder(oldPair.path)) return -1;
	  FolderEntry fe = fs.getEntry(oldPair.name);
	  if(fe == null) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	  
	  fs.changeCurFolder(path_backup);
	  
	  int addr = fe.addr;
	  
	  if(!fs.changeCurFolder(newPair.path)) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	  
	  if(!fs.createLink(newPair.name, addr)) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	  
	  fs.changeCurFolder(path_backup);
	  
	  return 0;
  }
  
  private int handleSymlink(int oldNameVMA, int newNameVMA) {
 
	  RealFileSystem fs = FilesysKernel.realFileSystem;
	  
	  String oldName = readVirtualMemoryString(oldNameVMA, MAXPATHLENGTH);
	  String newName = readVirtualMemoryString(newNameVMA, MAXPATHLENGTH);
	  
	  Pair newPair = fs.splitPath(newName);
	  
	  String path_backup = fs.getCurrentFolder();
	  
	  if(!fs.changeCurFolder(newPair.path)) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	  
	  if(!fs.createSymlink(newPair.name, oldName)) {
		  fs.changeCurFolder(path_backup);
		  return -1;
	  }
	  
	  fs.changeCurFolder(path_backup);
	  
	  return 0;
	  
  }
  
  public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
  {
    switch (syscall)
    {
      case SYSCALL_MKDIR:
      {
    	  Lib.debug('h',"handleMkdir");
    	  return handleMkdir(a0);
      }
        
      case SYSCALL_RMDIR:
      {
    	  Lib.debug('h',"handleRmdir");
    	  return handleRmdir(a0);
      }
        
      case SYSCALL_CHDIR:
      {
    	  Lib.debug('h',"handleChdir");
    	  return handleChdir(a0);
      }
        
      case SYSCALL_GETCWD:
      {
    	  Lib.debug('h',"handleGetcwd");
    	  return handleGetcwd(a0, a1);
      }
        
      case SYSCALL_READDIR:
      {
    	  Lib.debug('h',"handleReaddir");
    	  return handleReaddir(a0, a1, a2, a3);
      }
        
      case SYSCALL_STAT:
      {
    	  Lib.debug('h',"handleStat");
    	  return handleStat(a0, a1);
      }
       
      case SYSCALL_LINK:
      {
    	  Lib.debug('h',"handleLink");
    	  return handleLink(a0, a1);
      }
      
      case SYSCALL_SYMLINK:
      {
    	  Lib.debug('h',"handleSymlink");
    	  return handleSymlink(a0, a1);
      }
      
      default:
        return super.handleSyscall(syscall, a0, a1, a2, a3);
    }
  }
  
  private Folder cur_folder_backup;
  private LinkedList<String> cur_path_backup;
  
  //private String cur_path;
  
  private static final int MAXPATHLENGTH = 4096;
  private static final char dbgFS = 'q';
}
