package nachos.vm;

import nachos.machine.Coff;
import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.KThread;
import nachos.userprog.UserKernel;
import nachos.userprog.UserProcess;
import nachos.vm.VMKernel.Pair;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
		
		TLBbackup = new TranslationEntry[Machine.processor().getTLBSize()];
		TLBpidBackup = new int[Machine.processor().getTLBSize()];
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		
		Lib.debug(dbgVM, KThread.currentThread()+"saveState!");
		
		super.saveState();
		
		for(int i=0; i<Machine.processor().getTLBSize(); ++i) {
			TLBbackup[i] = Machine.processor().readTLBEntry(i);
			TLBpidBackup[i] = VMKernel.tlbScheduler.processID[i];
			Lib.debug(dbgVM, TLBbackup[i]+", "+TLBpidBackup[i]);
		}
		
		hasSaved = 1;
		
		VMKernel.tlbScheduler.invalidateAllTLB();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		
		Lib.debug(dbgVM, KThread.currentThread()+"restoreState!");
		
		if(hasSaved == 0) return;
		
		for(int i=0; i<Machine.processor().getTLBSize(); ++i) {
			TranslationEntry entry = TLBbackup[i];
			if(entry.valid && TLBpidBackup[i] >= 0) {
				int ppn = entry.ppn;
				Lib.debug(dbgVM, ppn+": "+VMKernel.pageScheduler.ipt.getPid(ppn)+"=?"+TLBpidBackup[i]
						+", "+VMKernel.pageScheduler.ipt.getVpn(ppn)+"=?"+entry.vpn);
				if(VMKernel.pageScheduler.ipt.getPid(ppn) != TLBpidBackup[i] || 
						VMKernel.pageScheduler.ipt.getVpn(ppn) != entry.vpn) {
					entry.valid = false;
				}
			}
			Machine.processor().writeTLBEntry(i, entry);
			VMKernel.tlbScheduler.processID[i] = TLBpidBackup[i];
			Lib.debug(dbgVM, entry+", "+TLBpidBackup[i]);
		}
		
		hasSaved = 0;
	}
	
	// override
	protected TranslationEntry getTranslationEntry(int vpn) {
		if(vpn < 0 || vpn >= numPages) return null;
		return VMKernel.pageScheduler.findPage(loader, pid, vpn);
	}
	
	// override
	protected void freeUpSpatialResources() {
		VMKernel.tlbScheduler.invalidateAllTLB();
		VMKernel.pageScheduler.clearAllPages(pid, numPages);
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		loader = new LazyLoader(coff);
		return loader.init();
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		super.unloadSections();
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionTLBMiss:
			VMKernel.tlbScheduler.handleTLBMiss(loader, pid, processor.pageFromAddress(processor.readRegister(Processor.regBadVAddr)));
			break;
		default:
			super.handleException(cause);
			break;
		}
	}
	
	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
	
	private TranslationEntry[] TLBbackup;
	private int[] TLBpidBackup;
	private int hasSaved;
	
	//phase 3
	LazyLoader loader;
	
	class LazyLoader {
		LazyLoader(Coff coff) {
			this.coff = coff;
			
			locatedSection = new CoffSection[numPages];
			sectionOffset = new int[numPages];
		}
		
		boolean init() {
			int count = 0;
			
			// load sections
			// for each section
			for (int s = 0; s < coff.getNumSections(); s++) {
				CoffSection section = coff.getSection(s);

				Lib.debug(dbgVM, "\tinitializing " + section.getName()
						+ " section (" + section.getLength() + " pages)");

				//for each page in this section
				for (int i = 0; i < section.getLength(); i++) {
					int vpn = section.getFirstVPN() + i;
					
					locatedSection[vpn] = section;
					sectionOffset[vpn] = i;
					++count;
				}
			}

			return true;
		}
		
		boolean loadNextSection(int vpn, int ppn) {
			if(vpn < 0 || vpn >= numPages) {
				Lib.debug(dbgVM, "LazyLoader.loadNextSection: out of range [0, "+numPages+")");
				return false;
			}
			
			CoffSection section = locatedSection[vpn];
			
			if(section == null) return true;	//nothing to load, maybe a stack page or something
			
			section.loadPage(sectionOffset[vpn], ppn);
			
			return true;
		}
		
		boolean isReadOnly(int vpn) {
			if(locatedSection[vpn] == null) return false;
			return locatedSection[vpn].isReadOnly();
		}
		
		public CoffSection[] locatedSection;
		public int[] sectionOffset;
		
		Coff coff;
	}
}
