package nachos.vm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import nachos.threads.Lock;
import nachos.filesys.RealFileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.ThreadedKernel;
import nachos.userprog.UserKernel;

/**
 * A kernel that can support multiple demand-paging user processes.
 */
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	public VMKernel() {
		super();
	}

	/**
	 * Initialize this kernel.
	 */
	public void initialize(String[] args) {
		super.initialize(args);
		
		if(tlbScheduler == null) {
			tlbScheduler = new TLBScheduler();
		}
		
		if(pageScheduler == null) {
			pageScheduler = new PageScheduler();
		}
	}

	/**
	 * Test this kernel.
	 */
	public void selfTest() {
		super.selfTest();
	}

	/**
	 * Start running user programs.
	 */
	public void run() {
		super.run();
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		System.out.println("Statistics:\n\tPageFaultCount = "+pageScheduler.pageFaultCount+"\n\tTlBMissCount = "+tlbScheduler.tlbMissCount);
		pageScheduler.swapfile.close();
		super.terminate();
	}

	private static final char dbgVM = 'v';
	
	//phase 3
	public static TLBScheduler tlbScheduler;
	public static PageScheduler pageScheduler;
	
	//a combination of processID pid and its virtual page number vpn
	class Pair {
		int pid, vpn;
		
		Pair(int pid, int vpn) {
			this.pid = pid;
			this.vpn = vpn;
		}
		public boolean equals(Object o) {
			Pair obj = (Pair) o;
			return pid == obj.pid && vpn == obj.vpn;
		}
		public int hashCode() {
			return pid + vpn;
		}
	}
	
	public class TLBScheduler {
		TLBScheduler() {
			processID = new int[Machine.processor().getTLBSize()];
		}
		
		void addTLBEntry(int pid, TranslationEntry entry) {
			
			int location = -1;
			
			for(int i=0; i< Machine.processor().getTLBSize(); ++i) {
				TranslationEntry tmp = Machine.processor().readTLBEntry(i);
				
				if(tmp.valid == false) {
					location = i; break;
				}
			}
			
			if(location < 0) {
				// need to substitute one TLB out,
				// random TLB replacement policy, to be implemented!
				
				int number = Lib.random(Machine.processor().getTLBSize());
				invalidateTLB(number);
				location = number;
			}
			
			Lib.debug(dbgVM, "TLBScheduler.addTLBEntry: schedule entry "+entry+" to "+location+"th TLB.");
			Machine.processor().writeTLBEntry(location, entry);
			processID[location] = pid;
			
			if(Lib.test(dbgVM)) printTLBcontent();
			
		}
		
		void dumpTLB(int ppn) {
			
			Lib.debug(dbgVM, "TLBScheduler.dumpTLB: "+ppn);
			
			if(Lib.test(dbgVM)) printTLBcontent();
			
			for(int i=0; i<Machine.processor().getTLBSize(); ++i) {
				TranslationEntry entry = Machine.processor().readTLBEntry(i);
				if(entry.valid && entry.ppn == ppn) {
					invalidateTLB(i);
				}
			}
		}
		
		void invalidateAllTLB() {
			
			Lib.debug(dbgVM, "TLBScheduler.invalidateAllTLB:.");
			
			for(int i=0; i<Machine.processor().getTLBSize(); ++i) {
				invalidateTLB(i);
			}
			
		}
		
		void invalidateTLB(int number) {
			Lib.debug(dbgVM, "TLBScheduler.invalidateTLB: NUMBER = "+number);
			// first to dump it
			TranslationEntry entry = Machine.processor().readTLBEntry(number);
			if(!entry.valid) return;
			pageScheduler.setPageEntry(processID[number], entry);
			
			// then invalidate it
			TranslationEntry tmp = new TranslationEntry();
			tmp.valid = false;
			processID[number] = -1;
			Machine.processor().writeTLBEntry(number, tmp);
		}
		
		void handleTLBMiss(VMProcess.LazyLoader loader, int pid, int vpn) {
			
			lock.acquire();
			
			Lib.debug(dbgVM, "TLBMiss1: "+pid+", "+vpn);
			++tlbMissCount;
			Lib.debug(dbgVM, "TLBMiss2: "+pid+", "+vpn);
			TranslationEntry entry = pageScheduler.findPage(loader, pid, vpn);
			Lib.debug(dbgVM, "TLBMiss3: "+pid+", "+vpn);
			addTLBEntry(pid, entry);
			Lib.debug(dbgVM, "TLBMiss4: "+pid+", "+vpn);
			
			lock.release();
		}
		
		int[] processID;
		public int tlbMissCount = 0;
		
		Lock lock = new Lock();
		
		// only for debugging
		void printTLBcontent() {
			for(int i=0; i<Machine.processor().getTLBSize(); ++i) {
				TranslationEntry entry = Machine.processor().readTLBEntry(i);
				System.out.println("TLB"+i+": PID = "+processID[i]+" VPN = "+entry.vpn+" PPN = "+entry.ppn+" VALID = "+entry.valid);
			}
		}
	}
	
	public class PageScheduler {
		PageScheduler() {
			ipt = new invertedPageTable();
		}
		
		TranslationEntry findPage(VMProcess.LazyLoader loader, int pid, int vpn) {
			
			lock.acquire();
			
			//ipt.printMapping();
			int ppn = ipt.find(new Pair(pid, vpn));
			Lib.debug(dbgVM, "PageScheduler.findPage: find (PID, VPN) = ("+pid+", "+vpn+"), PPN = "+ppn);
			
			if(ppn >= 0) {
				
				//the page is in main memory, just return it
				Lib.debug(dbgVM, "PageScheduler.findPage: PPN "+ppn+" is in main memory!");
				//ipt.printMapping();
				
				ipt.lru[ppn] = Machine.timer().getTime();
				
				return ipt.getEntry(ppn);
				
			} else {
				// the page is not in main memory, search it in swapfile or executable file, and load it to mm
				
				++pageFaultCount;
				
				if(swapfile.locatedInSwapfile(pid, vpn)) {
					// pages in swapfile
					Lib.debug(dbgVM, "PageScheduler.findPage: PPN "+ppn+" is in swapfile!");
					
					byte[] content = swapfile.readPage(pid, vpn);
					
					if(content == null) {
						Lib.debug(dbgVM, "PageScheduler.findPage: content is null!");
						lock.release();
						return null;
					}
					
					TranslationEntry entry =  ipt.allocateFreePage(loader, pid, vpn);
					
					System.arraycopy(content, 0, memory, entry.ppn * pageSize, pageSize);
					
					ipt.lru[entry.ppn] = Machine.timer().getTime();
					
					lock.release();
					return entry;
					
				} else {
					// pages in executable file
					Lib.debug(dbgVM, "PageScheduler.findPage: PPN "+ppn+" is in executable file!");
					
					TranslationEntry entry =  ipt.allocateFreePage(loader, pid, vpn);
					
					ipt.lru[entry.ppn] = Machine.timer().getTime();
					
					if(!loader.loadNextSection(vpn, entry.ppn)) {
						ipt.releasePage(entry.ppn);
						lock.release();
						return null;
					}
					
					lock.release();
					return entry;
				}
			}
		}
		
		void clearAllPages(int pid, int largestPN) {
			Lib.debug(dbgVM, "PageScheduler.clearAllPages: PID = "+pid);
			
			swapfile.clearAllPages(pid, largestPN);
			ipt.clearAllPages(pid, largestPN);
		}
		
		void setPageEntry(int pid, TranslationEntry entry) {
			ipt.setPageEntry(pid, entry);
		}
		
		invertedPageTable ipt;
		
		class invertedPageTable {
			invertedPageTable() {
				coreMap = new Pair[length];
				lru = new long[length];
				translates = new TranslationEntry[length];
				mapping = new HashMap<Pair, Integer>();
				freePhysicalPages = new LinkedList<Integer>();
				for(int i=0; i<length; ++i) {
					freePhysicalPages.add(new Integer(i));
				}
			}
			
			void clearAllPages(int pid, int largestPN) {
				for(int i=0; i<largestPN; ++i) {
					Integer res = mapping.get(new Pair(pid, i));
					if(res != null) {
						releasePage(res.intValue());
					}
				}
			}
			
			void setPageEntry(int pid, TranslationEntry entry) {
				
				Lib.debug(dbgVM, "InvertedPageTable.setPageEntry: mapping: "+entry);
				if(Lib.test(dbgVM)) printMapping();
			
				Integer res = mapping.get(new Pair(pid, entry.vpn));
				if(res == null) {
					Lib.debug(dbgVM, "InvertedPageTable.setPageEntry: there is no corresponding page in memory! Something must be wrong! PID: "+pid+" VPN: "+entry.vpn);
					Lib.assertNotReached();
				}
				int ppn = res.intValue();
				translates[ppn] = entry;
			}
			
			void releasePage(int ppn) {
				if(translates[ppn] == null) {
					Lib.debug(dbgVM, "InvertedPageTable.releasePage: no ppn "+ppn+" is being used!");
					return;
				}
				
				Lib.debug(dbgVM, "PageScheduler.releasePage: PPN = "+ppn);
				// check whether ppn is in TLB
				tlbScheduler.dumpTLB(ppn);
				
				int vpn = coreMap[ppn].vpn;
				int pid = coreMap[ppn].pid;
				
				mapping.remove(new Pair(pid, vpn));
				coreMap[ppn] = new Pair(-1, -1);
				
				// if dirty, write back
				if(translates[ppn].valid && translates[ppn].dirty) {
					byte[] content = new byte[pageSize];
					System.arraycopy(memory, ppn * pageSize, content, 0, pageSize);
					swapfile.writePage(pid, vpn, content);
				}
				
				translates[ppn] = null;
				lru[ppn] = 0;
				
				Lib.debug(dbgVM, "InvertedPageTable.releasePage: release ppn = "+ppn);
				
				freePhysicalPages.add(new Integer(ppn));
			}
			
			TranslationEntry allocateFreePage(VMProcess.LazyLoader loader, int pid, int vpn) {
				int ppn;
				
				Lib.debug(dbgVM, "InvertedPageTable.allocateFreePage: freePhysicalPages:"+freePhysicalPages);
				//if(Lib.test(dbgVM)) printMapping();
				
				if(freePhysicalPages.isEmpty()) {
					
					ppn = substituteLRU(pid, vpn);
					//ppn = substitute(pid, vpn);
					
					Lib.debug(dbgVM, "InvertedPageTable.allocateFreePage: VICTIM = "+ppn);
					freePhysicalPages.remove(new Integer(ppn));
				} else {
					Integer res = freePhysicalPages.removeFirst();
					ppn = res.intValue();
				}
				
				Lib.debug(dbgVM, "InvertedPageTable.allocateFreePage: freePhysicalPages2:"+freePhysicalPages);
				//if(Lib.test(dbgVM)) printMapping();
				Lib.debug(dbgVM, "InvertedPageTable.allocateFreePage: PID = "+pid+" VPN = "+vpn+" allocatedPPN = "+ppn);
				
				coreMap[ppn] = new Pair(pid, vpn);
				translates[ppn] = new TranslationEntry(vpn, ppn, true, loader.isReadOnly(vpn), false, false);
				mapping.put(new Pair(pid, vpn), new Integer(ppn));
				
				//if(Lib.test(dbgVM)) printMapping();
				
				return translates[ppn];
			}
			
			// use random picking
			int substitute(int pid, int vpn) {
				
				int victim = Lib.random(length);
				
				Lib.debug(dbgVM, "InvertedPageTable.substitute: PID = "+pid+" VPN = "+vpn+" VICTIM = "+victim);
		
				releasePage(victim);
				
				Lib.debug(dbgVM, "InvertedPageTable.substitute: PID = "+pid+" VPN = "+vpn+" VICTIM = "+victim);
				
				return victim;
			}
			
			// use LRU algorithm
			int substituteLRU(int pid, int vpn) {
				
				int victim = 0;
				for(int i=1; i<length; ++i) {
					if(lru[i] < lru[victim]) victim = i;
				}
					
				Lib.debug(dbgVM, "InvertedPageTable.substitute: PID = "+pid+" VPN = "+vpn+" VICTIM = "+victim);
					
				releasePage(victim);
							
				Lib.debug(dbgVM, "InvertedPageTable.substitute: PID = "+pid+" VPN = "+vpn+" VICTIM = "+victim);
							
				return victim;
			}
			
			TranslationEntry getEntry(int ppn) {
				if(ppn < 0 || ppn >= length) {
					Lib.debug(dbgVM, "InvertedPageTable.getEntry: ppn "+ppn+"out of range [0, "+length+")");
					return null;
				}
				
				lock.release();
				return translates[ppn];
			}
			
			int getPid(int ppn) {
				if(ppn < 0 || ppn >= length) {
					Lib.debug(dbgVM, "InvertedPageTable.getPid: ppn "+ppn+"out of range [0, "+length+")");
					return -1;
				}
				return coreMap[ppn].pid;
			}
			
			int getVpn(int ppn) {
				if(ppn < 0 || ppn >= length) {
					Lib.debug(dbgVM, "InvertedPageTable.getVpn: ppn "+ppn+"out of range [0, "+length+")");
					return -1;
				}
				return coreMap[ppn].vpn;
			}
			
			int find(Pair p) {
				Integer res = mapping.get(p);
				if(res == null) return -1;
				return res.intValue();
			}
			
			LinkedList<Integer> freePhysicalPages;
			
			int length = Machine.processor().getNumPhysPages();
			Pair[] coreMap;
			TranslationEntry[] translates;
			HashMap<Pair, Integer> mapping;
			
			long[] lru;
			
			// only for debugging
			void printMapping() {
				Iterator<Pair> ite = mapping.keySet().iterator();
				while(ite.hasNext()) {
					Pair tmp = ite.next();
					System.out.println("("+tmp.pid+", "+tmp.vpn+") -> "+mapping.get(tmp));
				}
			}
		}
		
		SwapFile swapfile = new SwapFile();
		Lock lock = new Lock();
		
		public int pageFaultCount = 0;
	}
	
	class SwapFile {
		SwapFile() {
			file = ThreadedKernel.fileSystem.open(SwapFileName, true);
			//file = Machine.stubFileSystem().open(SwapFileName, true);
		}
		
		void writePage(int pid, int vpn, byte[] content) {
			Lib.debug(dbgVM, "SwapFile.writePage: PID = "+pid+" VPN = "+vpn);
			
			Integer res = looksUpTable.get(new Pair(pid, vpn));
			
			int pageID;
			
			if(res == null) {
				// need to create new place to put this page
				pageID = allocatePage();
				looksUpTable.put(new Pair(pid, vpn), pageID);
			} else {
				// just cover the original data
				pageID = res.intValue();
			}
			
			file.write(pageSize * pageID, content, 0 ,pageSize);
		}

		void close() {
			file.close();
			if(!(ThreadedKernel.fileSystem instanceof RealFileSystem)) {
				ThreadedKernel.fileSystem.remove(SwapFileName);
			}
		}
		
		void clearAllPages(int pid, int largestPN) {
			for(int i=0; i<largestPN; ++i) {
				Integer res = looksUpTable.get(new Pair(pid, i));
				if(res != null) {
					releasePage(res.intValue());
				}
			}
		}
		
		void releasePage(int pageID) {
			vacantPages.add(new Integer(pageID));
		}
		
		boolean locatedInSwapfile(int pid, int vpn) {
			Integer res = looksUpTable.get(new Pair(pid, vpn));
			return res != null;
		}
		
		byte[] readPage(int pid, int vpn) {
			Integer res = looksUpTable.get(new Pair(pid, vpn));
			if(res == null) return null;
			
			byte[] ans = new byte[pageSize];
			int count = file.read(pageSize * res.intValue(), ans, 0, pageSize);
			
			if(count != pageSize) {
				Lib.debug(dbgVM, "SwapFile.readPage: not read fully "+pageSize+"character!");
				return null;
			}
			return ans;
		}
		
		int allocatePage() {
			if(vacantPages.isEmpty()) {
				return totalPages++;
			}
			return vacantPages.removeFirst().intValue();
		}
		
		int totalPages = 0;
		LinkedList<Integer> vacantPages  = new LinkedList<Integer>();
		
		OpenFile file;
		HashMap<Pair, Integer> looksUpTable = new HashMap<Pair, Integer>();
	}
	
	static final byte[] memory = Machine.processor().getMemory();
	static final int pageSize = Processor.pageSize;
	static final String SwapFileName = "SWAP";
}
